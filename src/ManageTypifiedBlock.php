<?php

namespace Drupal\typified_blocks;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides typified block manager.
 */
class ManageTypifiedBlock {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ManageTypifiedBlock object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns rendered block content.
   *
   * @param string $type
   *   The block type.
   *
   * @return array
   *   The renderable array representing the block content.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function render(string $type) {
    $block = [];

    $block_id = $this->entityTypeManager
      ->getStorage('block_content')
      ->getQuery()
      ->condition('field_type', $type)
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($block_id)) {
      $block_entity = $this->entityTypeManager
        ->getStorage('block_content')
        ->load(reset($block_id));

      if ($block_entity) {
        $block = $this->entityTypeManager
          ->getViewBuilder('block_content')
          ->view($block_entity);
      }
    }

    return $block;
  }

}
